<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
if(isset($_GET['logout'])){
    unset($_SESSION['user']);
}

if(isset($_SESSION['user'])) {
	echo "<div class='user'>Logged in as: ".$_SESSION['user']['username']." <a href='?logout'>Logout</a></div>";
}
else {
	echo "<div class='user'>&nbsp;</div>";
}
?>