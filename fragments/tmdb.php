<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
echo '
<main>
	<h1>The Movie Database (TMDb)</h1>';

if($action==1) {
	echo '<form method="get">
		<input type="hidden" id="menu" name="menu" value="5">
		<label for="search">Search:</label><br>
		<input type="text" id="search" name="search" required><br>
		<label for="category">Category:</label><br>
			<select name="category" id="category" required>
				<option value="movie">Movie</option>
				<option value="person">Person</option>
			</select><br><br>
		<input type="submit" value="Submit">
	</form><br>';
	
	if (isset($_GET['search']) && isset($_GET['category'])) {
	$search = $_GET['search'];
	$category = $_GET['category'];
	
	if($category=='movie') {
		$json = file_get_contents("https://api.themoviedb.org/3/search/movie?api_key=c6b07eb2a6230726c83b7c0ba9786567&language=en-US&query=".urlencode($search)."&page=1&include_adult=false");
		$data=array();
		$data = json_decode($json, true);
		$results = $data['results'];
		
		echo "<table>
			<tr>
				<th>Title</th>
				<th>Release date</th>
			</tr>";
		
		foreach($results as $movie) {
			echo "<tr>
						<td><a href='index.php?menu=5&action=2&id=".$movie['id']."'>".$movie['title']."</a></td>";
						$time = strtotime($movie['release_date']);
						$newformat = date('d.m.Y.',$time);
						echo "<td>".$newformat."</td>
				</tr>";
		}
		
		echo "</table>";
	}
	
	if($category=='person') {
		$json = file_get_contents("https://api.themoviedb.org/3/search/person?api_key=c6b07eb2a6230726c83b7c0ba9786567&language=en-US&query=".urlencode($search)."&page=1&include_adult=false");
		$data=array();
		$data = json_decode($json, true);
		$results = $data['results'];
		
		echo "<table>
			<tr>
				<th>Full name</th>
				<th>Gender</th>
				<th>Known for</th>
			</tr>";
		
		foreach($results as $person) {
			echo "<tr>
						<td><a href='index.php?menu=5&action=3&id=".$person['id']."'>".$person['name']."</a></td>";
						switch($person['gender']) {
							case 0:
								echo "<td>Not specified</td>";
								break;
							case 1:
								echo "<td>Female</td>";
								break;
							case 2:
								echo "<td>Male</td>";
								break;
							default:
								echo "<td></td>";
								break;
						}
						echo "<td>".$person['known_for_department']."</td>";
			echo "</tr>";
		}
		
		echo "</table>";
	}
}
}

if($action==2 && isset($_GET['id'])) {
	$id = $_GET['id'];
	
	$json = file_get_contents("https://api.themoviedb.org/3/movie/".$id."?api_key=c6b07eb2a6230726c83b7c0ba9786567&language=en-US");
	$data=array();
	$data = json_decode($json, true);
	
	echo "<img src='https://image.tmdb.org/t/p/w342".$data['poster_path']."' />";
	
	echo "<p>Title: ".$data['title']."</p>";
	echo "<p>Tagline: ".$data['tagline']."</p>";
	echo "<p>Runtime: ".$data['runtime']." minutes</p>";
	$time = strtotime($data['release_date']);
	$newformat = date('d.m.Y.',$time);
	echo "<p>Release date: ".$newformat."</p>";
	echo "<p>Overview:<br>".$data['overview']."</p>";
}

if($action==3 && isset($_GET['id'])) {
	$id = $_GET['id'];
	
	$json = file_get_contents("https://api.themoviedb.org/3/person/".$id."?api_key=c6b07eb2a6230726c83b7c0ba9786567&language=en-US");
	$data=array();
	$data = json_decode($json, true);
	
	echo "<img src='https://image.tmdb.org/t/p/w342".$data['profile_path']."' />";
	
	echo "<p>Name: ".$data['name']."</p>";
	$time = strtotime($data['birthday']);
	$newformat = date('d.m.Y.',$time);
	echo "<p>Birthday: ".$newformat."</p>";
	echo "<p>Place of birth: ".$data['place_of_birth']."</p>";
	echo "<p>Biography:<br>".$data['biography']."</p>";	
}
	
echo '</main>';
?>