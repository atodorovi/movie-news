<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
print '
<main>
	<h1>Register</h1>
	<form method="post">
		<label for="fname">First name:</label><br>
		<input type="text" id="fname" name="fname" required minlength="3" maxlength="30"><br>
		<label for="lname">Last name:</label><br>
		<input type="text" id="lname" name="lname" required minlength="3" maxlength="30"><br>
		<label for="email">Email:</label><br>
		<input type="email" id="email" name="email" required><br>
		
		<label for="country">Country:</label><br>
			<select name="country" id="country" required>
				<option value="" disabled selected>Select country</option>';
				$query  = "SELECT * FROM countries";
				$result = mysqli_query($MySQL, $query);
				while($row = mysqli_fetch_array($result)) {
					print '<option value="' . $row['country_code'] . '">' . $row['country_name'] . '</option>';
				}
			print '
			</select><br>
		
		<label for="username">Username:</label><br>
		<input type="text" id="username" name="username" required minlength="5" maxlength="30"><br>
		<label for="password">Password:</label><br>
		<input type="password" id="password" name="password" required minlength="5" maxlength="30"><br><br>
		<input type="submit" value="Submit">
	</form>';
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$query = 'INSERT INTO users (first_name, last_name, email, country, username, password) values(?,?,?,?,?,?)';
		$statement = mysqli_prepare($MySQL,$query);
		
		$password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT, ['cost' => 12]);
	 
		mysqli_stmt_bind_param($statement,'ssssss',
		$_POST['fname'],
		$_POST['lname'],
		$_POST['email'],
		$_POST['country'],
		$_POST['username'],
		$password_hash);
	 
		$account_created = mysqli_stmt_execute($statement);
		if($account_created==true) echo '<p>Account created.</p>';
		else echo '<p>Username or email is already taken.</p>';
	}
echo '</main>';


?>