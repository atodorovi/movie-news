<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
if($id>0 && $action==2 && ($is_editor || $is_admin)) {
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$query = '
		UPDATE
			news
		SET
			title=?,
			content=?,
			archive=?
		WHERE
			id=?
		';
		$statement = mysqli_prepare($MySQL,$query);
		mysqli_stmt_bind_param($statement,'ssss',
		$_POST['title'],
		$_POST['content'],
		$_POST['archive'],
		$id);
		mysqli_stmt_execute($statement);
		
	}
	
	$query  = "
	SELECT
		id,title,content,archive
	FROM
		news
	WHERE
		id=$id
	ORDER BY
		publish_date
	DESC
	";
	$result = mysqli_query($MySQL, $query);
	$row = mysqli_fetch_array($result);
	$archive = $row['archive'];
	$yes = $row['archive']=='Y' ? "checked" : "";
	$no = $row['archive']=='N' ? "checked" : "";
	
	echo '<h2>Edit article</h2>';
	echo '<form class="news-form" method="post">
			<label for="title">Title:</label><br>
			<input type="text" id="title" name="title" required value="'.$row['title'].'"><br>
			<label for="content">Content:</label><br>
			<textarea rows="25" name="content" required>'.$row['content'].'</textarea><br>
			<label for="archive">Archive:</label><br>
			<input type="radio" id="Y" name="archive" value="Y" '.$yes.'>
			<label for="Y">Yes</label>
			<input type="radio" id="N" name="archive" value="N" '.$no.'>
			<label for="N">No</label><br><br>
			<input type="submit" value="Update">
	</form>';
}
if($is_editor || $is_admin) {
	if($id>0 && $action==3 && $is_admin) {
		$query = "SELECT file_path FROM images WHERE news_id=$id";
		$result = mysqli_query($MySQL, $query);
		while($row = mysqli_fetch_array($result)) {
			unlink($row['file_path']);
		}
		
		$query = "DELETE FROM images WHERE news_id=$id";
		mysqli_query($MySQL, $query);
		
		$query = "DELETE FROM news WHERE id=$id";
		mysqli_query($MySQL, $query);
	}
	
	if($id==0) {
		echo '<h2>Manage articles</h2>';
		echo "<table>
			<tr>
				<th>Title</th>
				<th>Publish date</th>
				<th>Archive</th>";
				if($is_editor || $is_admin) echo "<th></th>";
				if($is_admin) echo "<th></th>";
		echo	"</tr>";
			
			$query  = "
			SELECT
				id,title,
				DATE_FORMAT(publish_date, '%d.%m.%Y.') AS p_date,
				archive
			FROM
				news
			ORDER BY
				publish_date
			DESC
			";
			$result = mysqli_query($MySQL, $query);
			while($row = mysqli_fetch_array($result)) {
				echo "<tr>
						<td>".$row['title']."</td>
						<td>".$row['p_date']."</td>
						<td>".$row['archive']."</td>";
						if($is_editor || $is_admin) {
							echo "<td><a href='index.php?menu=8&action=2&id=".$row['id']."'>Edit</a></td>";
						}
						if($is_admin) {
							echo "<td><a href='index.php?menu=8&action=3&id=".$row['id']."'>Delete</a></td>";
						}
				echo "</tr>";
			}
		echo "</table>";
	}
}
?>