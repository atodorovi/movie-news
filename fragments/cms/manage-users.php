<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
if($id>0 && $action==4 && $is_admin) {
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$query = '
		UPDATE
			users
		SET
			first_name=?,
			last_name=?,
			email=?,
			country=?,
			username=?,
			role=?,
			enabled=?	
		WHERE
			id=?
		';
		$statement = mysqli_prepare($MySQL,$query);
		mysqli_stmt_bind_param($statement,'ssssssss',
		$_POST['first_name'],
		$_POST['last_name'],
		$_POST['email'],
		$_POST['country'],
		$_POST['username'],
		$_POST['role'],
		$_POST['enabled'],
		$id);
		mysqli_stmt_execute($statement);
		
	}
	
	$query  = "
	SELECT
		*
	FROM
		users
	WHERE
		id=$id
	";
	$result = mysqli_query($MySQL, $query);
	$row = mysqli_fetch_array($result);
	$role = $row['role'];
	$country = $row['country'];
	$enabled = $row['enabled'];
	$yes = $enabled=='Y' ? "checked" : "";
	$no = $enabled=='N' ? "checked" : "";
	
	echo '<h2>Edit user</h2>';
	echo '<form class="user-form" method="post">
			<label for="first_name">First name:</label><br>
			<input type="text" id="first_name" name="first_name" required value="'.$row['first_name'].'"><br>
			
			<label for="last_name">Last name:</label><br>
			<input type="text" id="last_name" name="last_name" required value="'.$row['last_name'].'"><br>
			
			<label for="email">Email:</label><br>
			<input type="email" id="email" name="email" required value="'.$row['email'].'"><br>';
			
	echo 	'<label for="country">Country:</label><br>
			<select name="country" id="country" required>
				<option value="" disabled>Select country</option>';
				$query2  = "SELECT * FROM countries";
				$result2 = mysqli_query($MySQL, $query2);
				while($row2 = mysqli_fetch_array($result2)) {
					if($row2['country_code']==$country) {
						print '<option value="' . $row2['country_code'] . '" selected>' . $row2['country_name'] . '</option>';
					} else {
						print '<option value="' . $row2['country_code'] . '">' . $row2['country_name'] . '</option>';
					}
				}		
	echo 	'</select><br>';
			
	echo	'<label for="username">Username:</label><br>
			<input type="text" id="username" name="username" required value="'.$row['username'].'"><br>';
			
	echo 	'<label for="role">Role:</label><br>
			<select name="role" id="role" required>';
			$roles = ['user','editor','admin'];
			foreach ($roles as $value) {
				if($value==$role) {
					print '<option value="' . $value . '" selected>' . $value . '</option>';
				} else {
					print '<option value="' . $value . '">' . $value . '</option>';
				}
			}		
	echo 	'</select><br>';
			
	echo 	'<label for="enabled">Enabled:</label><br>
			<input type="radio" id="Y" name="enabled" value="Y" '.$yes.'>
			<label for="Y">Yes</label>
			<input type="radio" id="N" name="enabled" value="N" '.$no.'>
			<label for="N">No</label><br><br>
			
			<input type="submit" value="Update">
	</form>';
}
if($id==0 && $is_admin) {
	echo '<h2>Manage users</h2>';
		echo "<table>
			<tr>
				<th>Id</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Email</th>
				<th>Country</th>
				<th>Username</th>
				<th>Role</th>
				<th>Account enabled</th>
				<th></th>";
		echo	"</tr>";
			
			$query  = "
			SELECT
				*
			FROM
				users
			";
			$result = mysqli_query($MySQL, $query);
			while($row = mysqli_fetch_array($result)) {
				echo "<tr>
						<td>".$row['id']."</td>
						<td>".$row['first_name']."</td>
						<td>".$row['last_name']."</td>
						<td>".$row['email']."</td>
						<td>".$row['country']."</td>
						<td>".$row['username']."</td>
						<td>".$row['role']."</td>
						<td>".$row['enabled']."</td>";
						if($is_admin) {
							echo "<td><a href='index.php?menu=8&action=4&id=".$row['id']."'>Edit</a></td>";
						}
				echo "</tr>";
			}
		echo "</table>";
}
?>