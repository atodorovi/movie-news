<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
echo '<h2>Create article</h2>';
echo '<form class="news-form" method="post" enctype="multipart/form-data">
					<label for="title">Title:</label><br>
					<input type="text" id="title" name="title" required><br>
					<label for="content">Content:</label><br>
					<textarea rows="25" name="content" required></textarea><br>
					<label for="thumbnail">Thumbnail image:</label><br>
					<input type="file" name="thumbnail" id="thumbnail" required><br>
					<label for="thumbnail">Additional images:</label><br>
					<input type="file" name="images[]" id="images[]" multiple><br><br>
					<input type="submit" value="Submit">
	</form>';
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$news_id = null;
		
		if(isset($_FILES['thumbnail'])){
			$file_name = $_FILES['thumbnail']['name'];
			$file_size = $_FILES['thumbnail']['size'];
			$file_tmp = $_FILES['thumbnail']['tmp_name'];
			$file_type = $_FILES['thumbnail']['type'];
			$file_ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
			$file_path = "content/".time().'_'.$file_name;

			$extensions = array("jpeg","jpg","png");

			if(in_array($file_ext,$extensions)=== false){
				echo '<p>'."'$file_name'".' file type is incorrect. Only .jpg and .png allowed.</p>';
			}
			else if($file_size > 1048576) {
				echo '<p>'."'$file_name'".' file size too large. Maximum 1MB allowed.</p>';
			}
			else {
				$query = 'INSERT INTO news (author, title, content) values(?,?,?)';
				$statement = mysqli_prepare($GLOBALS['MySQL'],$query);
				$author = $_SESSION['user']['firstname'].' '.$_SESSION['user']['lastname'];
				mysqli_stmt_bind_param($statement,'sss',
				$author,
				$_POST['title'],
				$_POST['content']);
				mysqli_stmt_execute($statement);
				$news_id = mysqli_insert_id($GLOBALS['MySQL']);
				
				
				$thumbnail = 'Y';
				move_uploaded_file($file_tmp,$file_path);
				$query = 'INSERT INTO images (news_id, file_path, thumbnail) values(?,?,?)';
				$statement = mysqli_prepare($GLOBALS['MySQL'],$query);
				mysqli_stmt_bind_param($statement,'sss',
				$news_id,
				$file_path,
				$thumbnail);
				mysqli_stmt_execute($statement);
				
				echo '<p>News article created.</p>';
			}
	   }
	   
		foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name) {
			$file_name = $_FILES['images']['name'][$key];
			$file_size = $_FILES['images']['size'][$key];
			$file_tmp = $_FILES['images']['tmp_name'][$key];
			$file_type = $_FILES['images']['type'][$key];
			$file_ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));
			$file_path = "content/".time().'_'.$file_name;

			$extensions = array("jpeg","jpg","png");
			
			if($file_size>0) {
				if(in_array($file_ext,$extensions)=== false){
				echo '<p>'."'$file_name'".' file type is incorrect. Only .jpg and .png allowed.</p>';
				}
				else if($file_size > 1048576) {
					echo '<p>'."'$file_name'".' is too large. Maximum 1MB allowed.</p>';
				}
				else {
					move_uploaded_file($file_tmp,$file_path);
					
					$thumbnail = 'N';
					$query = 'INSERT INTO images (news_id, file_path, thumbnail) values(?,?,?)';
					$statement = mysqli_prepare($GLOBALS['MySQL'],$query);
					mysqli_stmt_bind_param($statement,'sss',
					$news_id,
					$file_path,
					$thumbnail);
					mysqli_stmt_execute($statement);
				}
			}
		}
	}
?>