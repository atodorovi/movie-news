<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
print "
<main>
			<h1>Your number one source for movie and television news!</h1>
			<p>Every week, you can expect a blend of the best movie, news, and television content from the team at MovieNews.</p>
			<h2>Here you will find:</h2>
			
			<div class='home-content'>
				<figure>
					<img src='img/henry-cavill.jpg' alt='Henry Cavill' title='Henry Cavill'/>
					<figcaption>Henry Cavill interview about The Witcher series.</figcaption>
				</figure>
				<p>Exclusive interviews with trending actors! We ask the hard questions, silly questions and fan questions and we don't stop until we get the answers. Learn what you always wanted to know about your favorite movie actors.</p>
			</div>
			
			<div class='home-content'>
				<figure>
					<img src='img/cannes.jpg' alt='Cannes Film Festival' title='Cannes Film Festival'/>
					<figcaption>Cannes Film Festival in 2018.</figcaption>
				</figure>
				<p>Latest reports from world-famous movie festivals! Find out the latest movies of the greatest movie directors of our time.</p>
			</div>
			
			<div class='home-content'>
				<figure>
					<img src='img/behind-the-scenes.jpg' alt='Behind the scenes of a movie' title='Behind the scenes of making a movie'/>
					<figcaption>Behind the scenes of making a movie.</figcaption>
				</figure>
				<p>Never before seen footage of behind the scenes! Learn how movies are made and what goes into the creative process.</p>
			</div>
		</main>"
?>