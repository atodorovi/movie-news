<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
print '
<main>
	<h1>Login</h1>
	<form method="post">		
		<label for="lname">Username:</label><br>
		<input type="text" id="username" name="username" required><br>
		<label for="password">Password:</label><br>
		<input type="password" id="password" name="password" required><br><br>
		<input type="submit" value="Submit">
	</form>';
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$query = '
		SELECT
			*
		FROM
			users
		WHERE
			username=? AND enabled="Y"
		';
		$statement = mysqli_prepare($MySQL,$query);
		mysqli_stmt_bind_param($statement,'s',$_POST['username']);
		mysqli_stmt_execute($statement);
		$result = mysqli_stmt_get_result($statement);
		$row = mysqli_fetch_array($result);
		
		if($row && password_verify($_POST['password'], $row['password'])) {
			$_SESSION['user']['id'] = $row['id'];
			$_SESSION['user']['role'] = $row['role'];
			$_SESSION['user']['username'] = $row['username'];
			$_SESSION['user']['firstname'] = $row['first_name'];
			$_SESSION['user']['lastname'] = $row['last_name'];
			
			header("Location: index.php?menu=8");
		}
		else {
			echo '<p>Incorrect credentials or account is not enabled by administrator.</p>';
		}
	}
echo '</main>';
?>