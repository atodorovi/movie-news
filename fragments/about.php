<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
print '
<main>
			<h1>About MovieNews</h1>
			<video controls>
				<source src="video/video.webm" type="video/webm">
			</video>
			<p>MovieNews is the premiere online destination for movie enthusiasts, providing the latest news and information on casting and development, release dates, trailers, interviews and clips, full movies and more, MovieNews keeps users connected to all their favorites, past, present and future. As a top social entertainment network, MovieNews is revolutionizing the way you DISCOVER, WATCH and DISCUSS the movies you love. Online since 2020, MovieNews is one of the last movie sites to appear on the Web. Covering the industry from our teams all around the world, MovieNews continues to be one of the most trusted destinations for the connected generation.</p>
		</main>'
?>