<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
$is_user = $_SESSION['user']['role']=='user';
$is_editor = $_SESSION['user']['role']=='editor';
$is_admin = $_SESSION['user']['role']=='admin';


echo '<nav>
		<ul class="menu">
			<li><a href="index.php?menu=8&action=1">Create article</a></li>';
			if($is_editor || $is_admin) {
			echo '<li><a href="index.php?menu=8&action=2">Manage articles</a></li>';
			}
			if($is_admin) {
			echo '<li><a href="index.php?menu=8&action=4">Manage users</a></li>';	
			}
echo '</ul>
	</nav>';

echo '
<main>
	<h1>CMS</h1>';
	
	switch ($action) {
		case 1:
			include 'fragments/cms/news-form.php';
			break;
		case 2:
			include 'fragments/cms/manage-news.php';
			break;
		case 3:
			include 'fragments/cms/manage-news.php';
			break;
		case 4:
			include 'fragments/cms/manage-users.php';
			break;
		default:
			break;
	}
	
echo '</main>';
?>