<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
echo '<head>
		<title>Movie News</title>
        <link rel="stylesheet" href="css/style.css">
		<meta charset="UTF-8">
		<meta name="keywords" content="movies, news, latest">
		<meta name="description" content="News about upcoming movies">
		<meta name="author" content="Andrej Todorović">		
		<link rel="icon" type="image/png" href="img/favicon.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>';
?>