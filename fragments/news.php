<?php if(!defined('__APP__')) die("Hacker!"); ?>
<?php
echo '<main>';
	if($id>0) {
		$query  = "
		SELECT
			n.id,
			n.author,
			n.title,
			REPLACE(n.content,CHAR(13),' <br/>') as content,
			DATE_FORMAT(publish_date, '%d.%m.%Y.') AS publish_date,
			n.archive,
			i.file_path,
			i.thumbnail
		FROM
			news n
		INNER JOIN
			images i
		WHERE
			n.id = i.news_id AND i.thumbnail = 'Y' AND n.archive = 'N' AND n.id=$id
		";
		$result = mysqli_query($MySQL, $query);
		$row = mysqli_fetch_array($result);
		echo "
		<article>
			<div class='article-header'>
				<h1>".$row['title']."</h1>
				<span>Published: ".$row['publish_date']."</span>
			</div>
			<div>
				<img src='".$row['file_path']."'>
				<p>".$row['content']."</p>
				<br>
				<span>Author: ".$row['author']."</span>
			</div>";
			
			echo "<div class='article-images'>";
		
			$query  = "
			SELECT
				i.file_path,
				i.alt,
				i.thumbnail
			FROM
				images i
			WHERE
				i.news_id=$id AND i.thumbnail = 'N'
			";
			$result = mysqli_query($MySQL, $query);
			while($row = mysqli_fetch_array($result)) {
				echo "<img src='".$row['file_path']."'></img>";
			}
			
			echo "</div>";
			
	echo "</article>";
	}
	else {
		echo '<h1>News</h1>';
		$query  = "
		SELECT
			n.id,
			n.author,
			n.title,
			SUBSTRING(content, 1, 300) AS content,
			DATE_FORMAT(publish_date, '%d.%m.%Y.') AS publish_date,
			n.archive,
			i.file_path,
			i.thumbnail
		FROM
			news n
		INNER JOIN
			images i
		WHERE
			n.id = i.news_id AND i.thumbnail = 'Y' AND n.archive = 'N'
		ORDER BY
			n.publish_date
		DESC
		";
		$result = mysqli_query($MySQL, $query);
		while($row = mysqli_fetch_array($result)) {
			echo "
			<article>
				<div class='article-header'>
					<h2><a href='index.php?menu=2&id=".$row['id']."'>".$row['title']."</a></h2>
					<span>Published: ".$row['publish_date']."</span>
				</div>
				<div class='article-content'>
					<a href='index.php?menu=2&id=".$row['id']."'>
					<img class='thumbnail' src='".$row['file_path']."'></a>
					<p>".$row['content']." <a href='index.php?menu=2&id=".$row['id']."'>More</a></p>
				</div>
			</article>";
			}
	}		
echo '</main>';
?>