<?php
	define('__APP__', TRUE);

	$menu = isset($_GET['menu']) ? (int)$_GET['menu'] : 1;
	$action = isset($_GET['action']) ? (int)$_GET['action'] : 1;
	$id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
	
	include 'dbconnect.php';
	
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <?php include 'fragments/head.php';?>
	
	<body class="container">
		<?php include 'fragments/header.php';?>
		
		<?php
			switch ($menu) {
				case 1:
					include 'fragments/home.php';
					break;
				case 2:
					include 'fragments/news.php';
					break;
				case 3:
					include 'fragments/contact.php';
					break;
				case 4:
					include 'fragments/about.php';
					break;
				case 5:
					include 'fragments/tmdb.php';
					break;
				case 6:
					include 'fragments/register.php';
					break;
				case 7:
					include 'fragments/login.php';
					break;
				case 8:
					include 'fragments/cms.php';
					break;
				default:
					break;
			}
		?>
		
		<?php include 'fragments/footer.php';?>
	</body>
</html>