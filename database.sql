CREATE DATABASE IF NOT EXISTS movie_news
CHARACTER SET utf8
COLLATE utf8_general_ci;

USE movie_news;

CREATE TABLE IF NOT EXISTS countries (
  id int AUTO_INCREMENT NOT NULL,
  country_code varchar(2) NOT NULL,
  country_name varchar(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS users (
  id int AUTO_INCREMENT NOT NULL,
  first_name varchar(30) NOT NULL,
  last_name varchar(30) NOT NULL,
  email varchar(30) NOT NULL,
  country varchar(2) NOT NULL,
  username varchar(30) NOT NULL,
  password varchar(100) NOT NULL,
  role ENUM('user', 'editor', 'admin') NOT NULL,
  enabled ENUM('N', 'Y') NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (email),
  UNIQUE (username)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS news (
  id int AUTO_INCREMENT NOT NULL,
  author varchar(60) NOT NULL,
  title varchar(255) NOT NULL,
  content TEXT NOT NULL,
  publish_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  archive ENUM('N', 'Y') NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS images (
  id int AUTO_INCREMENT NOT NULL,
  news_id int NOT NULL,
  file_path varchar(255) NOT NULL,
  alt varchar(255) NOT NULL DEFAULT "",
  thumbnail ENUM('N', 'Y') NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (news_id) REFERENCES news(id)
) ENGINE=InnoDB;

INSERT IGNORE INTO users(id,first_name,last_name,email,country,username,password,role,enabled) VALUES 
(1,'John','Doe','jdoe@mail.com','US','johndoe','$2y$12$JbhrdNl1hQgHBiKIPJxGJeVKe4yQpW7Hq/4N10LNMsTI6wHhaid82','user','Y'),
(2,'Dana','Scully','editor@mail.com','US','editor','$2y$12$RSGC/cYWvtSyGsFTMACOK.U7zn4dS0i9Mt8tAIvWo95vZ44WZiVRa','editor','Y'),
(3,'Fox','Mulder','admin@mail.com','US','admin','$2y$12$.XRA6lUv5H4QlyJdzHpFSeczbM8rhBobNimG/Ljvtj1YggMbjXqCC','admin','Y');

INSERT IGNORE INTO news (id, author, title, content, publish_date, archive) VALUES
(1, 'John Doe', 'World War Z: 6 Major Differences Between The Book And The Movie', 'True story, I once met World War Z author, Max Brooks, at Comic-Con. I got him to sign his new graphic novel at the time and since he seemed cordial enough, I asked him what he thought about the World War Z movie. He looked up at me while he was signing his autograph and told me that he thought it was a â€œgreat movie,â€ but that it wasnâ€™t his book. And that was my impression, too. The World War Z movie is fantastic, but itâ€™s nothing like the book at all.\r\n\r\nNow, this isnâ€™t necessarily a bad thing. There have been quite a few movie adaptations that are very different from their source material. And unlike somebody like Stephen King who canâ€™t stand the Stanley Kubrick version of his book, The Shining, Max Brooks seems pretty cool with the adaptation of World War Z as long as itâ€™s good. But in case you were wondering some of the bigger changes from the novel, Iâ€™ve compiled a list of just how far the movie deviated from the amazing book. And while I donâ€™t think this statement is even really necessary given what kind of list this is, but just in case youâ€™ve never seen the movie or read the book, major spoilers up ahead.\r\n\r\nWorld War Z the movie, if it had followed the book, was always going to be a challenge. Thatâ€™s because the full title of the novel is World War Z: An Oral History of the Zombie War. And the reason why this is such a challenge to adapt is because the book really is an oral history, meaning that the story is told through multiple sources discussing their role in what happened during (and after) the great Zombie War. In this way, there is no real central character like Brad Pitt in the movie.\r\n\r\nInstead, Max Brooks (yes, the author of the novel) is the interviewer and zombie expert since he previously wrote The Zombie Survival Guide, which is an actual book. Because of his expertise in all things zombie, in World War Z, he is also an agent of the United Nations Postwar Commission. So yes, itâ€™s super meta, which is why the screenwriters probably decided to just go with a straightforward narrative about a family trying to escape a bunch of running, ravenous zombies. Oh, and about that.\r\n\r\nOne of the defining features in the movie is all the ravenous, speedy zombies clamoring and climbing on top of each other to get to people. In fact, when I initially saw the trailer, this was the first sign I had that the movie would be completely different from the book, since the zombies in the novel act like the typical, shambling zombies you would see in something like in the George Romero movies or The Walking Dead.\r\n\r\n', '2020-11-25 16:24:55', 'N'),
(2, 'John Doe', '\'Cherry\': First Images, Release Date for Tom Holland\'s Dramatic Russo Bros. Reunion Revealed', 'New images from Tom Holland\'s upcoming movie Cherry have arrived, and they paint a dramatic portrait of what awaits viewers. Holland re-teamed with Avengers: Infinity War and Avengers: Endgame directing duo Joe and Anthony Russo for Cherry. The directing team brings to life a story based on the 2018 Nico Walker novel of the same name and from a script penned by Angela Russo-Otsot and Jessica Goldberg. Cherry follows the titular protagonist (Holland) as he goes from an Army medic in Iraq to a civilian dealing with PTSD who is caught up in a life of crime. Cherry also stars Ciara Bravo, Jack Reynor, Thomas Lennon, Michael Gandolfini, and Jeff Wahlberg.\r\n\r\nNew images from Cherry were released on Tuesday by Vanity Fair as part of an in-depth first look at Holland\'s new movie. While Holland is no stranger to playing a character who is in the midst of a personal crisis and is barely keeping their head above water (I\'m looking at you, The Devil All the Time), these new images from Cherry tease a real departure for the Marvel actor. The new photos also tease Cherry\'s relationship with Emily (Bravo), who proves to be a stabilizing force and reliable constant in Cherry\'s world as things crumble around him. Each of the new photos shares different parts of Cherry\'s story as he drifts from the Army to his hometown, where his life of crime escalates. Holland looks to be pushing into new territory with Cherry\'s intense story. In this writer\'s opinion, he never disappoints in a performance, so imagining what he\'ll tap into to make Cherry\'s story pop onscreen should prove to be a fun thought experiment ahead of its February 2021 release.', '2020-11-25 16:34:36', 'N');

INSERT IGNORE INTO images (id, news_id, file_path, alt, thumbnail) VALUES
(1, 1, 'content/1606321495_worldwarz.jpg', '', 'Y'),
(2, 1, 'content/1606321501_world_war_z_2.jpg', '', 'N'),
(3, 1, 'content/1606321505_world-war-z.jpg', '', 'N'),
(4, 1, 'content/1606321518_wwz.jpg', '', 'N'),
(5, 2, 'content/1606322076_cherry-tom-holland-agbo-soldier-phone-call.jpg', '', 'Y');

INSERT IGNORE INTO countries (id, country_code, country_name) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People''s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');